// 1. What directive is used by Node.js in loading the modules it needs?

		// require

// 2. What Node.js module contains a method for server creation?

		// http

// 3. What is the method of the http object responsible for creating a server using Node.js?

		// .createServer

// 4. What method of the response object allows us to set status codes and content types?
	
		// response.writeHead

// 5. Where will console.log() output its contents when run in Node.js?

		// Terminal

// 6. What property of the request object contains the address's endpoint?
		
		// port

const http = require("http");

const port = 3000;	

const server = http.createServer((request, response) => {
	if(request.url == '/login'){
		response.writeHead(200, {'Content-type': 'text/plain'});
		response.end('Welcome to the login page');
	}
	else{
		response.writeHead(404, {'Content-type': 'text/plain'});
		response.end('Sorry the page is not available');
	}
} );

server.listen(port);

console.log(`Server now is successfully running at localhost: ${port}`);	